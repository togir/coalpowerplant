# coalPowerPlant
Dieses Repo ist eine sammlung von Informationen für eine Schularbeit.

## Arbeitsauftrag:
1. Erklären Sie den Begriff konventionelle Energiegewinnung.
2. Erklären Sie die Funktionsweisen eines Kohlekraftwerkes. 
3. Nennen und erläutern Sie die verfahren der rauchgasentschwefelung.
4. Nennen und diskutieren Sie die Vor- und Nachteile eines Kohlekraftwerkes.
5. Diskutieren Sie, ob die die Abschaffung der Kohlekraftwerke eine Lösung für unser bevorstehende Klimakatastrophe sein können.
	
